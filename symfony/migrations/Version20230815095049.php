<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230815095049 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE positions (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(60) NOT NULL, surname VARCHAR(120) NOT NULL, email VARCHAR(120) NOT NULL, descriptions VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_position (id INT AUTO_INCREMENT NOT NULL, user_id_id INT NOT NULL, position_id_id INT NOT NULL, json_data VARCHAR(255) DEFAULT NULL, INDEX IDX_A6A100F59D86650F (user_id_id), INDEX IDX_A6A100F5F3847A8A (position_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_position ADD CONSTRAINT FK_A6A100F59D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_position ADD CONSTRAINT FK_A6A100F5F3847A8A FOREIGN KEY (position_id_id) REFERENCES positions (id)');
   
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_position DROP FOREIGN KEY FK_A6A100F5F3847A8A');
        $this->addSql('ALTER TABLE user_position DROP FOREIGN KEY FK_A6A100F59D86650F');
        $this->addSql('DROP TABLE positions');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_position');
    }
}
