<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\ExampleDataSeter\ExampleSetData;

class AddDataCommand extends Command
{
    protected static $defaultName = 'app:add-data';
    protected static $defaultDescription = 'Add few example data';

    private $entityMenager;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->entityMenager = $em;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        $dataSet=new ExampleSetData($this->entityMenager);
        $passUser=$dataSet->setData();
        if($dataSet->addedBefore){
            $io->success('Data Was ADDadded before, not now');
        }else{
            $io->success('Data Was ADD now user passwords-> '. $passUser[0].' ,for second user-> '.$passUser[1]);
        }
   

        return Command::SUCCESS;
    }
}
