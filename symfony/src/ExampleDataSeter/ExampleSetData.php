<?php
namespace App\ExampleDataSeter;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Entity\Positions;
use App\Entity\UserPosition;
use Doctrine\ORM\EntityManagerInterface;


class ExampleSetData{
    private $entityManager;
    public $addedBefore;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager=$entityManager;
        $this->addedBefore=false;
    }

    public function setData(){
        $is_tester = $this->entityManager->getRepository(Positions::Class)->findBy(array('name' => 'tester'));
        if(!empty($is_tester)){
            $this->addedBefore=true;
            return;
        }

        $user=new User('JAN','Nowak','jan@nowak.pl','brak');
        $user->genratedPassword();
        $user1=new User('Adam','Kowak','adam@kowak.pl','nic');
        $user1->genratedPassword();
        $this->entityManager->persist($user);
        $this->entityManager->persist($user1);
        $this->entityManager->flush();
        
        $postions=new Positions('tester');
        $postions1=new Positions('developer');
        $postions2=new Positions('project menager');

        

        $this->entityManager->persist($postions);
        $this->entityManager->persist($postions1);
        $this->entityManager->persist($postions2);
        $this->entityManager->flush();

        $userPostition=new UserPosition($user,$postions);
        $userPostition1=new UserPosition($user1,$postions1);
        $this->entityManager->persist($userPostition);
        $this->entityManager->persist($userPostition1);
        $this->entityManager->flush();
        return [$user->getTmpPassword(),$user1->getTmpPassword()];
    }

}