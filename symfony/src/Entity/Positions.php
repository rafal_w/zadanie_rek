<?php

namespace App\Entity;

use App\Repository\PositionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PositionsRepository::class)
 */
class Positions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=UserPosition::class, mappedBy="positionId", orphanRemoval=true)
     */
    private $Position;

    public function __construct($name)
    {
        $this->name=$name;
        $this->Position = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, UserPosition>
     */
    public function getPosition(): Collection
    {
        return $this->Position;
    }

    public function addPosition(UserPosition $position): self
    {
        if (!$this->Position->contains($position)) {
            $this->Position[] = $position;
            $position->setPositionId($this);
        }

        return $this;
    }

    public function removePosition(UserPosition $position): self
    {
        if ($this->Position->removeElement($position)) {
            // set the owning side to null (unless already changed)
            if ($position->getPositionId() === $this) {
                $position->setPositionId(null);
            }
        }

        return $this;
    }

    public function getFormatDataPosition():array
    {
        return [
            'id'=>$this->getId(),
            'name'=>$this->getName(),
        ];
    }


public function getTextsByNamePositons(){
        $texts=[
            "tester"=>[
                "text1"=>'Systemy testujące',
                "text2"=>'Systemy raportowe',
                "checkBox"=>'Zna selenium'
            ],
            "developer"=>[
                "text1"=>'Środowiska ide',
                "text2"=>'Języki programowania',
                "checkBox"=>'Zna mysql'
            ],
            "project menager"=>[
                "text1"=>'Metodologie prowadzenia projektów',
                "text2"=>'Systemy raportowe',
                "checkBox"=>'Zna scrum'
            ],
            "default"=>[
                "text1"=>'brak',
                "text2"=>'brak',
                "checkBox"=>'brak'
            ]
        ];
        return $texts[$this->getName()];
}

}
