<?php

namespace App\Entity;

use App\Repository\UserPositionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserPositionRepository::class)
 */
class UserPosition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $UserId;

    /**
     * @ORM\ManyToOne(targetEntity=Positions::class, inversedBy="id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $positionId;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $jsonData;


    public function __construct($user,$position,$jsonData='{"text1":"deafult","text2":"deafult","checkBox":true}'){
            $this->UserId=$user;
            $this->positionId=$position;
            $this->jsonData=$jsonData;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->UserId;
    }

    public function setUserId(?User $UserId): self
    {
        $this->UserId = $UserId;

        return $this;
    }

    public function getPositionId(): ?Positions
    {
        return $this->positionId;
    }

    public function positionId(): ?Positions
    {
        return $this->positionId;
    }

    public function setPositionId(?Positions $positionId): self
    {
        $this->positionId = $positionId;

        return $this;
    }

    public function getJsonData(): ?string
    {
        return $this->jsonData;
    }

    public function setJsonData(?string $jsonData): self
    {
        $this->jsonData = $jsonData;

        return $this;
    }

    public function dataDecode(){
        return json_decode($this->getJsonData());
    }

}
