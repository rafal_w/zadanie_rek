<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * #[UniqueEntity('email')]
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=120, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $descriptions;

    /**
     * @ORM\OneToMany(targetEntity=UserPosition::class, mappedBy="UserId", orphanRemoval=true)
     */
    private $PositionId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;
    
    private $tmpPassword;

    public function __construct($name,$surname,$email,$descriptions='')
    {
            $this->name=$name;
            $this->surname=$surname;
            $this->email=$email;
            $this->descriptions=$descriptions;
            $this->PositionId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDescriptions(): ?string
    {
        return $this->descriptions;
    }

    public function setDescriptions(string $descriptions): self
    {
        $this->descriptions = $descriptions;
        return $this;
    }

    /**
     * @return Collection<int, UserPosition>
     */
    public function getPositionId(): Collection
    {
        return $this->PositionId;
    }

    public function addPositionId(UserPosition $positionId): self
    {
        if (!$this->PositionId->contains($positionId)) {
            $this->PositionId[] = $positionId;
            $positionId->setUserId($this);
        }

        return $this;
    }

    public function removePositionId(UserPosition $positionId): self
    {
        if ($this->PositionId->removeElement($positionId)) {
            // set the owning side to null (unless already changed)
            if ($positionId->getUserId() === $this) {
                $positionId->setUserId(null);
            }
        }

        return $this;
    }


    public function getFormatDataUser():array
    {
        $new=[
            'userId'=>$this->getId(),
            'name'=>$this->getName(),
            'email'=>$this->getEmail(),
            'surname'=>$this->getSurname(),
            'descritpitons'=>$this->getDescriptions(),

        ];
        $tmpPositions=$this->getPositionId();
        foreach($tmpPositions as $ele){
            $new['postitions'][]=[
                    'jsonData'=>$ele->getJsonData(),
                    'idPosition'=>$ele->getPositionId()->getId(),
                    'positionName'=> $ele->getPositionId()->getName()
            ];
        }

        return $new;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function setTmpPassword(string $password): self
    {
        $this->tmpPassword = $password;

        return $this;
    }
    public function getTmpPassword(): string
    {

        return  $this->tmpPassword;
    }

    public function genratedPassword(){
        $length=8;
        $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
        '0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';
            $str = '';
            $max = strlen($chars) - 1;

            for ($i=0; $i < $length; $i++)
            $str .= $chars[random_int(0, $max)];

            $this->setTmpPassword($str);
            
            $this->setPassword(md5($str));
            
    }


}
