<?php
namespace App\MyEmail;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MyEmail{
    public function sendEmail(User $user,MailerInterface $mailer)
    {
        $email = (new TemplatedEmail())
            ->from('my@email.com')
            ->to($user->getEmail())
            ->subject('User Created')
            ->htmlTemplate('Mail/mail.html.twig')
                ->context([
                    'user' => $user,
                ]);

        $mailer->send($email);
    }


}




