<?php 
namespace App\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Positions;
use App\Entity\UserPosition;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class UserPositionsType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
        
        $builder
        // ->add('jsonData',TextType::class)
        ->add('id',HiddenType::class)
        ->add('positionId',EntityType::class,[
            'class'=>Positions::class,
            'choice_label'=>'name',   
            'label'=>'Position'
        ])
        ->add('text1', TextType::class, [
            'label' =>  isset($options['data']['text1_label']) ? $options['data']['text1_label'] : '' , 
            'required' => false,
            'mapped' => false ,
            'data' => isset($options['data']['text1']) ? $options['data']['text1'] : ''      
        ])
        ->add('text2', TextType::class, [
            'label' =>  isset($options['data']['text2_label']) ? $options['data']['text2_label'] : '' , 
            'required' => false,
            'mapped' => false ,
            'data' => isset($options['data']['text2']) ? $options['data']['text2'] : ''                
        ])
        ->add('checkbox', CheckBoxType::class, [
            'label' =>  isset($options['data']['checkBox_label']) ? $options['data']['checkBox_label'] : '',    
            'required' => false,
            'mapped' => false,
            'data'    => isset($options['data']['checkBox']) ? $options['data']['checkBox'] : false,                
        ])
        ->add('zapisz',SubmitType::class);
    }
}