<?php 
namespace App\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Positions;
use App\Entity\UserPosition;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class UserType extends AbstractType{

    
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('name',TextType::class)
        ->add('surname',TextType::class)
        //->add('id',HiddenType::class)
        ->add('email',TextType::class,[ 'disabled' => true,])
        ->add('descriptions',TextType::class)
        // ->add('Position',EntityType::class,[
        //     'class'=>Positions::class,
        //     'choice_label'=>'name',
        //     'mapped' => false
        
        // ])
        ->add('zapisz',SubmitType::class);
    }
}