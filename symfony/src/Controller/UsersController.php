<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Entity\Positions;
use App\Entity\UserPosition;
use App\MyEmail\MyEmail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Mailer\MailerInterface;

class UsersController extends AbstractController{


    /**
     * get all user from data base
     * @Route("/get-users", methods={"GET"})
     * @param EntityManagerInterface $entityMenager
    */
    public function getUsers( EntityManagerInterface $entityMenager, SerializerInterface $serializer): Response
    {
        $entityUsers=$entityMenager->getRepository(User::Class);
        $allUsers=$entityUsers->findAll();
        $status=202;
        $data=[];
        foreach($allUsers as $user){
            $data[]=$user->getFormatDataUser();
        }
            return new JsonResponse(['users' =>$serializer->normalize($data),'status'=>$status]);
    }

       
    /**
     * @param Request $request
     * @param EntityManagerInterface $entityMenager
     * @return Response
    * @Route("/create-user", methods={"POST"})
    */
    public function createUser(Request $request, EntityManagerInterface $entityMenager,MailerInterface $mailer):Response
    {
        // $response = new Response();
        // $response->setContent(json_encode([
        //     'id' => 1,
        //     'time' =>2 
        // ]));
        // $response->headers->set('Content-Type', 'application/json');
        // $response->headers->set('Access-Control-Allow-Origin', '*');
        // $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
         $parameters = json_decode($request->getContent(), true);
        if(empty($parameters['name'])|| empty($parameters['surname'])|| empty($parameters['email'])){
            return new JsonResponse(['msg' =>'data is empty!','status'=>500,'error'=>true]);
        }

        $is_existUser=$entityMenager->getRepository(User::Class)->findOneBy(array('email' => $parameters['email']));
        if($is_existUser){
            return new JsonResponse(['msg' =>'user exist with this email','userId'=>$is_existUser->getId(),'status'=>405,'error'=>true]);
        }

        $descriptions=!empty($parameters['descriptions'])?$parameters['descriptions']:'';


        $user=new User($parameters['name'],$parameters['surname'],$parameters['email'],$descriptions);
        $user->genratedPassword();

        if(!empty($parameters['positions'])  && !empty($parameters['positions']['postionName'])){
            $is_existPosition=$entityMenager->getRepository(Positions::Class)->findOneBy(array('name' => $parameters['positions']['postionName']));
            if(empty($is_existPosition)){
                return new JsonResponse(['msg' =>'this positions->['. $parameters['positions']['postionName'].'],dont exist in database ','status'=>406,'error'=>true]);
            }
            $entityMenager->persist($user);
            $entityMenager->flush();
            $userPostions=new UserPosition($user,$is_existPosition,$parameters['positions']['jsonData']);
            $entityMenager->persist($userPostions);
            $entityMenager->flush();
            $emailSend=new MyEmail();
            $emailSend->sendEmail($user,$mailer);

        }else{
            $entityMenager->persist($user);
            $entityMenager->flush();
        }

        return new JsonResponse(['msg' =>'user added ','id'=>$user->getId(),'status'=>202]);
    }



    /**
     * @param int $id
     * @param EntityManagerInterface $entityMenager
     * 
     * @return Response
    * @Route("/remove-user/{id}", methods={"DELETE"})
    */
    public function remove(int $id, EntityManagerInterface $entityMenager):Response
    {
        $repository=$entityMenager->getRepository(User::Class);
        $user=$repository->find($id);
        if(empty($user)){
            return new JsonResponse(['msg' =>'not found user','status'=>404]);
        }
        $entityMenager->remove($user);
         $entityMenager->flush();
            return new JsonResponse(['msg' =>'user deleted','status'=>202]);        
    }

}