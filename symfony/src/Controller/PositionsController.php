<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Positions;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;


class PositionsController extends AbstractController{

    /**
     * get all positions from data base
     * @Route("/get-positions", methods={"GET"})
     * @param EntityManagerInterface $entityMenager
    */
    public function getPositions( EntityManagerInterface $entityMenager, SerializerInterface $serializer): Response
    {
        $entityPositions=$entityMenager->getRepository(Positions::Class);
        $allUPosition=$entityPositions->findAll();
        $status=202;
        $data=[];
        foreach($allUPosition as $position){
            $data[]=$position->getFormatDataPosition();
        }

        return new JsonResponse(['position' =>$serializer->normalize($data),'status'=>$status]);
    }

}