<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Entity\Positions;
use App\Entity\UserPosition;
use App\Type\UserType;
use App\Type\UserPositionsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;


class AdminController extends AbstractController{
    /**
     * get all user from data base and display
     * @Route("/admin", methods={"GET"},name="list-admin")
     * @param EntityManagerInterface $entityMenager
    */
    public function getUsers( EntityManagerInterface $entityMenager, SerializerInterface $serializer): Response
    {
        $entityUsers=$entityMenager->getRepository(User::Class);
        $allUsers=$entityUsers->findAll();
        return $this->render('Admin/admin-users.html.twig',['users'=>$allUsers]);
    }

    /**
     * @param int $id
     * @param EntityManagerInterface $entityMenager
     * 
     * @return Response
    * @Route("/edit-user/{id}", methods={"GET","POST"})
    */
    public function editUser(int $id, EntityManagerInterface $entityMenager,Request $request):Response
    {
        $repository=$entityMenager->getRepository(User::Class);
        $user=$repository->find($id);
        if(empty($user)){
            return $this->redirect('/admin');
        }

            $form=$this->createForm(UserType::class,$user);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $entityMenager->persist($user);
                $entityMenager->flush();
                return $this->redirect('/admin');
            }


            $anotherFieldsData=$user->getPositionId()[0]->dataDecode();
            $textsLabel=$user->getPositionId()[0]->getPositionId()->getTextsByNamePositons();
            $positions=$user->getPositionId()[0];
            $savedId=$positions->getPositionId()->getId();

            $form2=$this->createForm(UserPositionsType::class, $positions,[
                'data'=>[
                    'text1'=>empty($anotherFieldsData->text1)?'':$anotherFieldsData->text1,
                    'text2'=>empty($anotherFieldsData->text2)?'':$anotherFieldsData->text2,
                    'checkBox'=> $anotherFieldsData->checkBox,
                    'text1_label'=> $textsLabel['text1'],
                    'text2_label'=> $textsLabel['text2'],
                    'checkBox_label'=> $textsLabel['checkBox'],
                    'idPodsitionSaved' => $savedId
            ],
            ]);


            $form2->handleRequest($request);
            if($form2->isSubmitted()){
                $text1 = $form2->get('text1')->getData();
                $text2 = $form2->get('text2')->getData();
                $checkBox = ($form2->get('checkbox')->getData()==1)?true:false;
                $positionId= $form2->get('positionId')->getData();

                    $is_existPosition=$entityMenager->getRepository(Positions::Class)->findOneBy(array('id' => $positionId));
                    if(empty($is_existPosition)){
                        return $this->redirect('/admin');
                    }

                    $jsonData=json_encode([
                        'text1'=>$text1,
                            'text2'=>$text2,
                            'checkBox'=>$checkBox
                    ]);

                    $positions->setPositionId($is_existPosition);
                    $positions->setJsonData($jsonData);
                    $entityMenager->persist($positions);
                    $entityMenager->flush();
                return $this->redirect('/admin');
            }


            return $this->render('Admin/edit-user.html.twig',['user'=>$user,'form'=>$form->createView(),
            'form2'=>$form2->createView()
        ]);
    }       
    

}