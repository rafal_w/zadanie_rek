document.addEventListener("DOMContentLoaded", () => {
  RemoveClassListnner();
  EditUserLocation();
});


function EditUserLocation(){
  let userElements=document.querySelectorAll(".edit-box");
  userElements.forEach((e) => {
    e.addEventListener("click", function () {
      let userId = this.getAttribute("data-id");
      const url = "http://localhost:8001/edit-user/" + userId;
      if (!userId) {
        return;
      }
      window.location.href=url;
    });
  });
}

function RemoveClassListnner() {
  let removeButtonElements = document.querySelectorAll(".remove-box");

  removeButtonElements.forEach((e) => {
    e.addEventListener("click", function () {
      let userId = this.getAttribute("data-id");
      if (!userId) {
        return;
      }
      const text = "Czy chcesz usunąć użytkownika?";
      if (confirm(text) == true) {
        removeUserById(userId).then((e) => {
          if (e) {
            alert("Użytkownik usunięty");
            this.parentElement.remove();
            const elementCount=document.querySelector('.count-users');
            console.log( elementCount.textContent);
            elementCount.textContent=elementCount.textContent-1;
          }
        });
      }
    });
  });
}

async function removeUserById(userId) {
  const url = "http://localhost:8001/remove-user/" + userId;
  const response = await fetch(url, {
    method: "DELETE",
  });
  const data = await response.json();
  if ((data.status = 202)) {
    return true;
  }

  return false;
}
