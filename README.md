git clone https://gitlab.com/rafal_w/zadanie_rek.git

/*
po pobraniu z gitlab, 
przechodzimy do katalgou w zadanie_rek a potem do /front i tam w konsoli urchamiamy polecenie(instalacja node modules,react):
npm install

*/

/*
w folderze zadanie_rek w terminalu urchamiamy (tworzenie dockera):
docker-compose up --build 
*/

/*
uruchomić w folderze zadanie_rek w terminalu komendę :
 docker exec -it  zadanie_rek-php-1 bash 
,następnie uruchomić konsoli obraz kontolera komendę(instalacjasmyfony i komponentow): 
  composer require symfony/web-server-bundle --dev
*/

/*
 migracja bazy dalej w konosli contraolera (zadanie_rek-php-1) :
php bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration


 ,wgranie danych przykładowych (potrzebne to w grania danych stanowiska dalej w kontrolerze zadanie_rek-php-1):
 php ./bin/console app:add-data
*/

/*
 linki 
 do maili ->http://localhost:8025/# ,
 lista użytkowinków backend symfony ->http://localhost:8001/admin ,
 fronted react dodawanie usera->http://localhost:3000 ,
*/