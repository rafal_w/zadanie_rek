import React, { Component, useState, useEffect } from "react";
import {
  StyledInput,
  StyledLabel,
  StyledDivInputGroup,
} from "./Styled/AnotherFieldsForm.styled";

const AnotherFieldsForm = (props) => {
  const handleChange = (event) => {
    let value = event.target.value;
    if (event.target.id == "checkBox") {
      value = event.target.checked;
    }
    props.callbackFromParentFields(value, event.target.id);
  };

  
  return (
    <div>
      <StyledDivInputGroup className="input-group">
        <StyledLabel htmlFor="text1">{props.texts[0]}</StyledLabel>
        <StyledInput
          type="text"
          name="text1"
          id="text1"
          onChange={handleChange}
          title="text"
          value={props.values[0]}
          required
        />
      </StyledDivInputGroup>
      <StyledDivInputGroup className="input-group">
        <StyledLabel htmlFor="text2">{props.texts[1]}</StyledLabel>
        <StyledInput
          type="text"
          name="text2"
          id="text2"
          value={props.values[1]}
          onChange={handleChange}
          title="text"
          required
        />
      </StyledDivInputGroup>
      <StyledDivInputGroup className="input-group">
        <StyledLabel htmlFor="checkbox">{props.texts[2]}</StyledLabel>
        <StyledInput
          type="checkbox"
          name="checkbox"
          id="checkBox"
          className="checkBox"
          checked={props.values[2]}
          onChange={handleChange}
          title="checkbox"
        />
      </StyledDivInputGroup>
    </div>
  );
};
export default AnotherFieldsForm;
