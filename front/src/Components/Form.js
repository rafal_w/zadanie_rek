import React, { Component } from "react";
import ReactDOM from "react-dom";
import Selects from "./Selects";
import AnotherFieldsForm from "./AnotherFieldsForm";
import Modal from "./Modal";
import styled from "styled-components";

import {
  StyledInput,
  StyledLabel,
  StyledButonn,
  StyledDiv,
  StyledForm,
  StyledDivInputGroup,
} from "./Styled/Form.styled";

class Form extends Component {
  myType = "";
  texts = [
    ["Systemy testujące", "Systemy raportowe", "Zna selenium"],
    ["Środowiska ide", "Języki programowania", "Zna mysql"],
    ["Metodologie prowadzenia projektów", "Systemy raportowe", "Zna scrum"],
    ["Text1", "Text2", "Checkbox"],
  ];
  constructor(props) {
    super(props);
    this.myType = this.texts[0];
    this.state = {
      values: {
        email: "",
        name: "",
        surname: "",
        descriptions: "",
      },
      anotherValues: {
        text1: "",
        text2: "",
        checkBox: false,
      },
      isSubmitting: false,
      isError: false,
      positions: "tester",
      isPositionsGet: false,
      msg: "",
    };
  }

  setAnotherFieldsValue = (value, id) => {
    this.setState({
      anotherValues: { ...this.state.anotherValues, [id]: value },
    });
  };

  chengedPositions = (name) => {
    this.setState({ positions: name });
    this.setState({
      anotherValues: {
        text1: "",
        text2: "",
        checkBox: false,
      },
    });
    switch (name) {
      case "tester":
        this.myType = this.texts[0];
        break;
      case "developer":
        this.myType = this.texts[1];
        break;
      case "project menager":
        this.myType = this.texts[2];
        break;
      default:
        this.myType = this.texts[3];
        break;
    }
  };
  setWasOptionsGet = (isGet) => {
    this.setState({ isPositionsGet: isGet });
  };

  formatDataToSend = () => {
    const myFormat = {
      ...this.state.values,
      positions: {
        jsonData: JSON.stringify(this.state.anotherValues),
        postionName: this.state.positions,
      },
    };
    return myFormat;
  };

  submitForm = async (e) => {
    e.preventDefault();
    this.setState({ isSubmitting: true });
    const data2 = this.state.values;

    const res = await fetch("http://localhost:8001/create-user", {
      method: "POST",
      body: JSON.stringify(this.formatDataToSend()),
      headers: {
        "Content-Type": "application/json",
      },
    });

    this.setState({ isSubmitting: false });
    const data = await res.json();

    !data.hasOwnProperty("error")
      ? this.setState({ msg: data.msg })
      : this.setState({ msg: data.msg, isError: true });
  };

  setIsClick = (isClick, correctData = false) => {
    if (isClick && !correctData) {
      this.setState({
        isError: false,
        msg: "",
        values: { email: "", name: "", surname: "", descriptions: "" },
        anotherValues: {
          text1: "",
          text2: "",
          checkBox: false,
        },
      });
    } else if (correctData) {
      this.setState({
        msg: "",
        isError: false,
      });
    }
  };

  handleInputChange = (e) =>
    this.setState({
      values: { ...this.state.values, [e.target.name]: e.target.value },
    });

  render() {
    return (
      <>
        <StyledDiv id="form-wrapper-main">
          <StyledForm onSubmit={this.submitForm}>
            <StyledDivInputGroup className="input-group">
              <StyledLabel htmlFor="email">E-mail Address</StyledLabel>
              <StyledInput
                type="email"
                name="email"
                id="email"
                value={this.state.values.email}
                onChange={this.handleInputChange}
                title="Email"
                required
              />
            </StyledDivInputGroup>
            <StyledDivInputGroup className="input-group">
              <StyledLabel htmlFor="name">Name</StyledLabel>
              <StyledInput
                type="text"
                name="name"
                id="name"
                value={this.state.values.name}
                onChange={this.handleInputChange}
                title="name"
                required
              />
            </StyledDivInputGroup>
            <StyledDivInputGroup className="input-group">
              <StyledLabel htmlFor="surname">Surname</StyledLabel>
              <StyledInput
                type="text"
                name="surname"
                id="surname"
                value={this.state.values.surname}
                onChange={this.handleInputChange}
                title="surname"
                required
              />
            </StyledDivInputGroup>
            <StyledDivInputGroup className="input-group">
              <StyledLabel htmlFor="descriptions">Descriptions</StyledLabel>
              <StyledInput
                type="text"
                name="descriptions"
                id="descriptions"
                value={this.state.values.descriptions}
                onChange={this.handleInputChange}
                title="descriptions"
              />
            </StyledDivInputGroup>
            {this.state.isPositionsGet ? null : (
              <p>Download Positions,Wait...</p>
            )}
            <StyledDivInputGroup
              className="input-group"
              style={{ display: this.state.isPositionsGet ? "flex" : "none" }}
            >
              <StyledLabel htmlFor="Positions">Position:</StyledLabel>
              <Selects
                value={this.state.positions}
                callbackFromParentPosition={this.chengedPositions}
                callbackFromParentDataGet={this.setWasOptionsGet}
              />
            </StyledDivInputGroup>
            {this.state.isPositionsGet ? (
              <AnotherFieldsForm
                callbackFromParentFields={this.setAnotherFieldsValue}
                values={[
                  this.state.anotherValues.text1,
                  this.state.anotherValues.text2,
                  this.state.anotherValues.checkBox,
                ]}
                texts={this.myType}
              />
            ) : null}
            {!this.state.isSubmitting && (
              <StyledButonn
                type="submit"
                style={{
                  display: this.state.isPositionsGet ? "block" : "none",
                }}
              >
                Create User
              </StyledButonn>
            )}
            <div>{this.state.isSubmitting ? "Adding User, Please Wait..." : this.state.msg}</div>
          </StyledForm>
        </StyledDiv>
        {this.state.msg != "" && (
          <Modal
            setIsClick={this.setIsClick}
            msg={this.state.msg}
            isError={this.state.isError}
          />
        )}
      </>
    );
  }
}

export default Form;
