import React, { Component, useState, useEffect } from "react";
import { StyledSelect } from "./Styled/Selects.styled";

import axios from "axios";

const Users = (props) => {
  const url = "http://localhost:8001/get-positions";
  const [options, getOptions] = useState([]);
  const [data, getData] = useState([false]);

  const getAllPositions = () => {
    axios
      .get(`${url}`)
      .then((res) => {
        const allPositions = res.data.position;
        console.log(res.data.position);
        getOptions(allPositions);
        getData(true);
        props.callbackFromParentDataGet(true);
      })
      .catch((error) => console.error(`Error: ${error}`));
  };

  useEffect(() => {
    getAllPositions();
  }, []);

  const handleChange = (event) => {
    props.callbackFromParentPosition(event.target.value);
  };

  return (
    <div>
      <div id="options-positions">
        <div className="select-container">
          <StyledSelect onChange={handleChange}>
            {options.map((option) => (
              <option value={option.name} key={option.id}>
                {option.name}
              </option>
            ))}
          </StyledSelect>
        </div>
      </div>
    </div>
  );
};
export default Users;
