import styled from "styled-components";
export const StyledInput = styled.input`
  font-size: 15px;
  color: #232;
  padding: 2px 5px;
  &:active,
  &:focus {
    border: 1px solid gray;
  }
`;
export const StyledLabel = styled.label`
  padding: 0px 10px 0 10px;
  max-width: 200px;
  width: 100%;
`;

export const StyledDivInputGroup = styled.div`
  display: flex;
  margin-bottom: 20px;
  max-width: 500px;
  width: 100%;
  justify-content: space-between;
`;

export const StyledForm = styled.form`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const StyledDiv = styled.div`
  padding: 20px 10px;
  display: flex;
  max-width: 800px;
  width: 100%;
  border: 1px solid #999;
  border-radius: 3px;
  align-items: center;
  justify-content: center;
  transitions: 1s east all;
  background-color: rgba(244, 240, 242, 0.8);
`;


export const StyledButonn = styled.button`
  text-transform: uppercase;
  font-weight: 600;
  color: white;
  border: 1px solid #228b22;
  border-radius: 4px;
  outline: none;
  background-color: #4caf50;
  padding: 5px 10px;
  margin-top: 30px;
  &:hover {
    background-color: #6ecf72;
  }
  &:active {
    outline: none;
    background: #6ecf72;
    border: 1px solid #006400;
  }
  &:focus {
    outline: none;
    background: #6ecf72;
    border: 1px solid #006400;
  }
`;
