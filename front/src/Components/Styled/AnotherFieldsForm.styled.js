import styled, { css } from "styled-components";
export const StyledInput = styled.input`
  font-size: 15px;
  color: #232;
  padding: 2px 5px;
  &:active,
  &:focus {
    border: 1px solid gray;
  }
  
  ${(props) =>
    props.className === "checkBox" &&
    css`
      width: 20px;
      height: 20px;
    `};
`;
export const StyledLabel = styled.label`
  padding: 0px 10px 0 10px;
  max-width: 200px;
  width: 100%;
`;

export const StyledDivInputGroup = styled.div`
  display: flex;
  margin-bottom: 20px;
  max-width: 500px;
  width: 100%;
  justify-content: space-between;
`;
