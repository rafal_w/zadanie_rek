import styled from "styled-components";
export const StyledSelect = styled.select`
  font-size: 15px;
  color: #232;
  max-width:200px;
  width:195px;
  padding: 2px 5px;
  &:active,
  &:focus {
    border: 1px solid gray;
  }
`;
