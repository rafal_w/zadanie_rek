import styled from "styled-components";
export const StyledDivModal = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0, 0, 0, 0.5);
  & > div {
    background-color: #fff;
    padding: 20px;
    max-width: 300px;
    width: 100%;
    max-height: 300px;
    border-radius: 3px;
    -webkit-box-shadow: -1px 0px 13px -1px rgba(0, 0, 0, 1);
    -moz-box-shadow: -1px 0px 13px -1px rgba(0, 0, 0, 1);
    box-shadow: -1px 0px 13px -1px rgba(0, 0, 0, 1);
  }
  & > div > div > div {
    display: flex;
    justify-content: space-between;
    align-items: self-start;
  }
  & > div > div > div > p {
    width: 100%;
    text-align: center;
    text-transform: uppercase;
  }
  & > div > div > div > p.ErrorText {
    color: red;
  }
  & > div > div > div > p.succes {
    color: green;
    padding-left: 30px;
  }
  & > div > div > div:nth-child(2) {
    flex-direction: column;
  }
  & > div > div > div:nth-child(3) {
    justify-content: center;
  }
`;


export const StyledModalButonn = styled.button`
  text-transform: uppercase;
  font-weight: 600;
  color: white;
  border: 1px solid #228b22;
  border-radius: 4px;
  outline: none;
  background-color: #4caf50;
  padding: 5px 10px;
  &:hover {
    background-color: #6ecf72;
  }
  &:active {
    outline: none;
    background: #6ecf72;
    border: 1px solid #006400;
  }
  &:focus {
    outline: none;
    background: #6ecf72;
    border: 1px solid #006400;
  }
`;
