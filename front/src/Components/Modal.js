import React from "react";
import { StyledDivModal, StyledModalButonn } from "./Styled/Modal.styled";

const Modal = (props) => {
  
  let isError = props.isError;
  return (
    <StyledDivModal>
      <div onClick={() => props.setIsClick(true, isError)}>
        <div>
          <div>
            <p className={isError ? "ErrorText" : "succes"}>
              {isError ? "Error" : "Success"}
            </p>
            <StyledModalButonn onClick={() => props.setIsClick(true, isError)}>
              x
            </StyledModalButonn>
          </div>

          <div>
            <p>{props.msg}</p>
            {isError && <p>Correct the email addres</p>}
          </div>
          <div>
            <div>
              <StyledModalButonn
                onClick={() => props.setIsClick(true, isError)}
              >
                OK
              </StyledModalButonn>
            </div>
          </div>
        </div>
      </div>
    </StyledDivModal>
  );
};

export default Modal;
